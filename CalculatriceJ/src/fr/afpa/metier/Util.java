package fr.afpa.metier;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Util {
	/**
	 * permet d'enregistrer l'historique
	 * 
	 * @param operation	l'operation a enregistrer
	 */
	public static void historique(String operation) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter("historique.txt",true));
			bw.write(operation);
		} catch (FileNotFoundException e) {
			System.out.println("fichier non trouv�");
		} catch (IOException e) {
			System.out.println("probleme d'entree / sortie");
		}finally {
			try {
				bw.close();
			} catch (IOException e) {
				System.out.println("probleme d'entree / sortie");
			}
		}
	}
	
	/**
	 * permet de lire l'historique
	 * 
	 * @return une chaine de character de l'historique
	 */
	public static String readHistorique() {
		historique("");
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader("historique.txt"));
			String all = "";
			while(br.ready()) {
				all += br.readLine()+"<br>";
			}
			return all;
		} catch (FileNotFoundException e) {
			System.out.println("fichier non trouv�");
		} catch (IOException e) {
			System.out.println("probleme d'entree / sortie");
		}finally {
			try {
				br.close();
			} catch (IOException e) {
				System.out.println("probleme d'entree / sortie");
			}
		}
		return null;
	}
}
