package fr.afpa.metier;

public class Calcul {
	
	public static boolean division;
	
	/**
	 * Calcul l'operation en entr�e
	 * 
	 * @param calcul la chaine de character de l'operation
	 * @return le resultat de l'operation
	 */
	public static float calculer(String calcul) {
		division = false;
		Util.historique(calcul);
		float resultat = nombre(calcul);
		while(calcul.length() > 0) {
			char op = calcul.charAt(0);
			calcul = calcul.substring(1);
			switch (op) {
			case '+':
				resultat += nombre(calcul);				
				break;
			case '-':
				resultat -= nombre(calcul);				
				break;
			case '*':
				resultat *= nombre(calcul);				
				break;
			case '/':
				float n = nombre(calcul);
				if(n != 0f)
					resultat /= n;
				else {
					division = true;
					Util.historique("=ERROR\n");
					return 0f;
				}
				break;
			default:
				break;
			}
		}
		Util.historique("="+resultat+"\n");
		return resultat;
	}
	
	/**
	 * trouve le premier nombre de la chaine de charactere
	 * 
	 * @param operation la chaine de charactere de l'operation
	 * @return le premier nombre en float
	 */
	private static float nombre(String operation) {
		int i = operation.length();
		while(i > 0) {
			if(operation.substring(0, i).matches("[0-9\\.]+")) {
				float res = Float.parseFloat(operation.substring(0, i));
				operation = operation.substring(i);
				return res;
			}
			else
				i--;
		}
		return 0f;
	}
}
