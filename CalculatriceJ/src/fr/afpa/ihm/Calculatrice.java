package fr.afpa.ihm;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import fr.afpa.metier.Calcul;
import fr.afpa.metier.Util;

public class Calculatrice implements ActionListener {
	public static JLabel l1;
	private JMenuItem mi2_1;

	public Calculatrice() {
		Util.historique("");
		JFrame f = new JFrame("Calculatrice");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Ajout Menu
		JMenuBar mb = new JMenuBar();
		JMenu m1 = new JMenu("Fichier");
		mb.add(m1);
		JMenuItem mi1 = new JMenuItem("clear");
		m1.add(mi1);
		mi1.addActionListener(this);
		JMenuItem mi2 = new JMenuItem("historique");
		m1.add(mi2);
		mi2.addActionListener(this);
		JMenuItem mi3 = new JMenuItem("quitter");
		m1.add(mi3);
		mi3.addActionListener(this);
		JMenu m2 = new JMenu("?");
		mb.add(m2);
		mi2_1 = new JMenuItem("infos appli");
		mi2_1.addActionListener(this);
		m2.add(mi2_1);

		// decoupage en 2 lignes
		JPanel p = new JPanel(new GridLayout(2, 1));
		// Ajout label pour afficher le calcul et le resultat
		l1 = new JLabel();
		l1.setText("0");
		p.add(l1);

		// Decoupage en 3 colonnes
		JPanel p2 = new JPanel(new GridLayout(1, 3));

		// Decoupage de la premiere colonne
		JPanel p2_1 = new JPanel(new GridLayout(3, 3));
		// ajout des boutons 1-9
		for (int i = 1; i < 10; i++) {
			JButton b = new JButton();
			b.setText("" + i);
			b.addActionListener(this);
			p2_1.add(b);
		}
		p2.add(p2_1);

		// Decoupage de la seconde colonne
		JPanel p2_2 = new JPanel(new GridLayout(3, 1));
		// ajout des boutons 1-9
		JButton b2_1 = new JButton();
		b2_1.setText("0");
		b2_1.addActionListener(this);
		p2_2.add(b2_1);

		JButton b2_2 = new JButton();
		b2_2.setText(".");
		b2_2.addActionListener(this);
		p2_2.add(b2_2);
		b2_2.setVisible(false);
		

		JButton b2_3 = new JButton();
		b2_3.setText("=");
		b2_3.addActionListener(this);
		p2_2.add(b2_3);
		p2.add(p2_2);

		// Decoupage de la troisieme colonne
		JPanel p2_3 = new JPanel(new GridLayout(4, 1));
		// ajout des boutons 1-9
		JButton b3_1 = new JButton();
		b3_1.setText("+");
		b3_1.addActionListener(this);
		p2_3.add(b3_1);
		JButton b3_2 = new JButton();
		b3_2.setText("-");
		b3_2.addActionListener(this);
		p2_3.add(b3_2);
		JButton b3_3 = new JButton();
		b3_3.setText("*");
		b3_3.addActionListener(this);
		p2_3.add(b3_3);
		JButton b3_4 = new JButton();
		b3_4.setText("/");
		b3_4.addActionListener(this);
		p2_3.add(b3_4);
		p2.add(p2_3);

		p.add(p2);

		f.setJMenuBar(mb);
		f.add(p);
		f.pack();
		f.setVisible(true);
	}

	public static void main(String[] args) {
		new Calculatrice();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if ("clear".equals(e.getActionCommand())) // si c'est le clear
			l1.setText("0");
		else if ("historique".equals(e.getActionCommand())) { // si c'est l'historique
			try {
				if ("".equals(Util.readHistorique()))
					l1.setText("aucun historique");
				else
					l1.setText("<HTML>" + Util.readHistorique());
			} catch (NullPointerException ex) {

			}
		} else if ("quitter".equals(e.getActionCommand())) // si c'est quitter
			System.exit(0);
		else if ("infos appli".equals(e.getActionCommand())) // si c'est infos appli
			JOptionPane.showMessageDialog(null, "Application développé par Dorne Julien");
		else if (!"=".equals(e.getActionCommand())) // si c'est une autre touche que "=" ou les precedentes
			if ("Error".equals(l1.getText()) || "0.0".equals(l1.getText()) || "0".equals(l1.getText()) || "aucun historique".equals(l1.getText())
					|| ("<HTML>" + Util.readHistorique()).equals(l1.getText()))
				l1.setText(e.getActionCommand());
			else
				l1.setText(l1.getText() + e.getActionCommand());
		else { // sinon
			try {
				float res = Calcul.calculer(l1.getText());
				if (!Calcul.division)
					l1.setText("" + res);
				else
					l1.setText("Error");
			} catch (NumberFormatException ex) {
				Util.historique("=ERROR\n");
				l1.setText("Error");
			}
		}
	}
}
