package fe.afpa.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.afpa.metier.Calcul;

public class TestCalcul {

	@Test
	public void testCalculer() {
		assertEquals(2f,Calcul.calculer("1+1"));
		assertEquals(3f,Calcul.calculer("1+1+1"));
		assertEquals(0f,Calcul.calculer("1-1"));
		assertEquals(1f,Calcul.calculer("1*1"));
		assertEquals(1f,Calcul.calculer("1/1"));
	}

}
