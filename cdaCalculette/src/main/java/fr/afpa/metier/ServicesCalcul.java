package fr.afpa.metier;

import javax.swing.JFrame;

public class ServicesCalcul {

	public static String calcul(String nombre1, String operateur, String nombre2) {
		float resultat;

		if (operateur.length() == 1) {
			switch (operateur) {
			case "+":
				resultat = Integer.parseInt(nombre1) + Integer.parseInt(nombre2);
				return "" + resultat;

			case "-":
				resultat = Integer.parseInt(nombre1) - Integer.parseInt(nombre2);
				return "" + resultat;

			case "*":
				resultat = Integer.parseInt(nombre1) * Integer.parseInt(nombre2);
				return "" + resultat;

			case "/":
				if (nombre2.equals("0")) {
					return "";
				} else {
					resultat = Integer.parseInt(nombre1) * Integer.parseInt(nombre2);
					return "" + resultat;
				}
			default: 
				return "";
			}
		} else
			return "";
	}
}
