package fr.afpa.ihm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class Menus {
	
	
public static JMenuBar creationBarreMenu() {
	// creation Menu bar
	JMenuBar menuBar = new JMenuBar();

	// creation chaque onglet
	JMenu menu1 = new JMenu("Fichier");

	JMenu menuInfo = new JMenu("?");

	// creation des element dans le deroulement du menu
	JMenuItem menuReset = new JMenuItem("Reset");
	JMenuItem menuHisto = new JMenuItem("Historique");
	JMenuItem menuQuit = new JMenuItem("Quitter");
	JMenuItem menuInfosAp = new JMenuItem("Info appli");

	// ajout de l'action quitter si on clique sur ce menu
	menuQuit.addActionListener(new ActionListener() {

		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	});

	// ajout dans le menu les elements deroulants
	menu1.add(menuReset);
	menu1.add(menuHisto);
	menu1.add(menuQuit);
	menuInfo.add(menuInfosAp);

	// ajout des onglets dans le menu
	menuBar.add(menu1);
	menuBar.add(menuInfo);
	return menuBar;
}
}
