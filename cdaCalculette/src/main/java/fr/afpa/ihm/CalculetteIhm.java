package fr.afpa.ihm;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import fr.afpa.metier.EcouteurFenetre;

public class CalculetteIhm extends JFrame implements ActionListener {
	private JLabel afficherOp;
	private float nb1;
	private float nb2;
	private String operateur;

	public void creationStructure() {
		this.setJMenuBar(Menus.creationBarreMenu());
		setSize(300, 300);
		setResizable(false);
	
		// creation interne calque
		JPanel panel1 = new JPanel();
		BorderLayout calqueNord = new BorderLayout();

		panel1.setLayout(calqueNord);

		panel1.add(creationduPanel(), BorderLayout.CENTER);
		panel1.add(new JLabel(" "), BorderLayout.WEST);
		panel1.add(new JLabel(" "), BorderLayout.EAST);
		afficherOp = new JLabel();
		addWindowListener(new EcouteurFenetre());
		
		
		
		
		panel1.add(afficherOp, BorderLayout.NORTH);
	
		add(panel1);
	
	
		setVisible(true);
		
	}

	/**
	 * Methode pour qui crée le panel avec les boutons
	 * 
	 * @return
	 */
	public JPanel creationduPanel() {
		JPanel panelBoutons = new JPanel();
		GridLayout gr = new GridLayout(3, 5);

		panelBoutons.setLayout(gr);
		// creation bouton chiffres
		JButton sept = new JButton("7");
		sept.addActionListener(this);
		
		JButton huit = new JButton("8");
		huit.addActionListener(this);
		
		JButton neuf = new JButton("9");
		neuf.addActionListener(this);
		
		JButton cinq = new JButton("5");
		cinq.addActionListener(this);
		
		JButton six = new JButton("6");
		six.addActionListener(this);
		
		JButton quatre = new JButton("4");
		quatre.addActionListener(this);
		
		JButton trois = new JButton("3");
		trois.addActionListener(this);
		
		JButton deux = new JButton("2");
		deux.addActionListener(this);
		
		JButton un = new JButton("1");
		un.addActionListener(this);
		
		JButton zero = new JButton("0");
		zero.addActionListener(this);

		// creation boutons operations
		JButton plus = new JButton("+");
		plus.addActionListener(this);
		
		JButton moins = new JButton("-");
		moins.addActionListener(this);
		
		JButton mult = new JButton("*");
		mult.addActionListener(this);
		
		JButton div = new JButton("/");
		div.addActionListener(this);
		
		JButton egal = new JButton("=");

		// Premiere Ligne
		panelBoutons.add(sept);
		panelBoutons.add(huit);
		panelBoutons.add(neuf);
		panelBoutons.add(plus);
		panelBoutons.add(moins);

		// Deuxieme Ligne
		panelBoutons.add(quatre);
		panelBoutons.add(cinq);
		panelBoutons.add(six);
		panelBoutons.add(mult);
		panelBoutons.add(div);

		// Troisieme ligne
		panelBoutons.add(un);
		panelBoutons.add(deux);
		panelBoutons.add(trois);
		panelBoutons.add(zero);
		panelBoutons.add(egal);

		//ajout de listener au boutons
		
		return panelBoutons;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
				
		
		if(e.getActionCommand().matches("[1-9]{1,}")){
			
			afficherOp.setText(afficherOp.getText()+e.getActionCommand());
			
			nb1 = Float.parseFloat(afficherOp.getText()+e.getActionCommand());
			
//			if(e.getActionCommand().matches("[+\\-*/]")) {
//				afficherOp.setText(afficherOp.getText()+e.getActionCommand());
//			}
		}
		
	}

}
